
use bevy::{
    prelude::*,
    render::camera::OrthographicProjection,
    app::AppExit,
    window::PrimaryWindow,
    input::mouse::MouseWheel,
    utils::Duration,
};

use std::collections::HashSet;

use crate::components::{
    LiveCell,
    Coord,
};

use crate::consts::{
    GRID_SIZE,
    SPAWN_COLOR,
};

use crate::neighbours::{
    GridVector,
    CyclicEnum,
};

use crate::resources::{
    Pause,
    Grid,
    LifeRule,
    NSelect,
};

pub const NTIME: f32 = 3.0;

#[derive(Component)]
pub struct RText;

#[derive(Component)]
pub struct NText;

#[derive(Component)]
pub struct NTimer(pub Timer);

#[derive(Resource)]
pub struct Painting(pub bool);

pub fn camera_input_system(
    keyboard_input: Res<ButtonInput<KeyCode>>,
    mut scroll_evr: EventReader<MouseWheel>,
    wnd_query: Query<&Window, With<PrimaryWindow>>,
    mut camera_query: Query<(&mut Transform, &mut OrthographicProjection), With<OrthographicProjection>>,
) {
    let (mut camera_transform, mut camera_projection) = camera_query.single_mut();

    // moving camera
    if keyboard_input.pressed(KeyCode::ArrowRight) ||
        keyboard_input.pressed(KeyCode::KeyD) {
        camera_transform.translation.x += 10.0*camera_projection.scale;
    }
    if keyboard_input.pressed(KeyCode::ArrowLeft) ||
        keyboard_input.pressed(KeyCode::KeyA)  {
        camera_transform.translation.x -= 10.0*camera_projection.scale;
    }
    if keyboard_input.pressed(KeyCode::ArrowUp) ||
        keyboard_input.pressed(KeyCode::KeyW)  {
        camera_transform.translation.y += 10.0*camera_projection.scale;
    }
    if keyboard_input.pressed(KeyCode::ArrowDown) ||
        keyboard_input.pressed(KeyCode::KeyS)  {
        camera_transform.translation.y -= 10.0*camera_projection.scale;
    }
    if keyboard_input.pressed(KeyCode::KeyR) {
        camera_transform.translation.x = 0.0;
        camera_transform.translation.y = 0.0;
    }

    // zoom
    let Some(cp) = wnd_query.single().cursor_position() else {return};
    let Ok(primary_window) = wnd_query.get_single() else {return};
    let wnd_size = Vec2 {x: primary_window.width(), y: primary_window.height()};
    let p_before = (cp - wnd_size/2.0)*Vec2 { x: 1.0, y: -1.0}*camera_projection.scale + camera_transform.translation.truncate();
    for ev in scroll_evr.read() {
        if ev.y < 0.0 { camera_projection.scale *= 1.10 } else { camera_projection.scale /= 1.1 }
    }
    camera_projection.scale = camera_projection.scale.clamp(0.1, 10.0);
    let p_after = (cp - wnd_size/2.0)*Vec2 { x: 1.0, y: -1.0}*camera_projection.scale + camera_transform.translation.truncate();
    camera_transform.translation = camera_transform.translation + (p_before-p_after).extend(0.0);
}

pub fn speed_control_input_system(
    keyboard_input: Res<ButtonInput<KeyCode>>,
    mut fixed_time: ResMut<Time<Fixed>>,
) {
    if keyboard_input.pressed(KeyCode::Minus) {
        // KeyCode::Minus means Plus
        let t = fixed_time.delta().as_secs_f32();
        fixed_time.set_timestep(Duration::from_secs_f32(t/1.1))
    }
    if keyboard_input.pressed(KeyCode::Slash) {
        // KeyCode::Slash means Minus
        let t = fixed_time.delta().as_secs_f32();
        fixed_time.set_timestep(Duration::from_secs_f32(t*1.1))
    }
}

pub fn game_input_system(
    keyboard_input: Res<ButtonInput<KeyCode>>,
    mut pause: ResMut<Pause>,
    mut commands: Commands,
    query: Query<Entity, With<LiveCell>>,
    mut grid: ResMut<Grid>,
    mut app_exit_events: EventWriter<AppExit>,
) {
    if keyboard_input.just_pressed(KeyCode::Space) {
        pause.0 = !pause.0
    }
    if keyboard_input.just_pressed(KeyCode::KeyC) {
        for entity in query.iter() {
            commands.entity(entity).despawn();
        }
        grid.0 = HashSet::new();
    }
    if keyboard_input.just_pressed(KeyCode::Escape)
        || keyboard_input.just_pressed(KeyCode::KeyQ)
        || keyboard_input.just_pressed(KeyCode::KeyX)
    {
        app_exit_events.send(AppExit);
    }
}

pub fn neighbourhood_input_system(
    mut commands: Commands,
    keyboard_input: Res<ButtonInput<KeyCode>>,
    mut nhood: ResMut<NSelect>,
    mut rule: ResMut<LifeRule>,
    mut query: Query<(Entity, &mut Text, &mut NTimer), With<NText>>,
    time: Res<Time>,
) {
    if keyboard_input.just_pressed(KeyCode::KeyN) ||
        keyboard_input.just_pressed(KeyCode::KeyV)
    {
        for (entity, _, _) in query.iter() {
            commands.entity(entity).despawn();
        }
        if keyboard_input.just_pressed(KeyCode::KeyN) {
            nhood.0.next();
        }
        commands.spawn(
            TextBundle::from(nhood.0.string().as_str())
                .with_style(
                    Style {
                        position_type: PositionType::Absolute,
                        top: Val::Px(5.0),
                        left: Val::Px(15.0),
                        ..default()
                    }
                )
            )
            .insert(NText)
            .insert(NTimer(Timer::from_seconds(NTIME, TimerMode::Once)));
        if nhood.0.max() < rule.die_upper {
            rule.die_upper = nhood.0.max();
            if rule.die_lower > rule.die_upper {
                rule.die_lower = rule.die_upper
            }
        }
        if rule.live > nhood.0.max() {
            rule.live = nhood.0.max()
        }
    } else {
        for (entity, mut text, mut timer) in query.iter_mut() {
            timer.0.tick(time.delta());
            if timer.0.finished() {
                commands.entity(entity).despawn();
            } else {
                text.sections[0].style.color = Color::Rgba {
                    red: 1.0,
                    green: 1.0,
                    blue: 1.0,
                    alpha: 1.0 * ( timer.0.fraction_remaining() ),
                };
            }
        }
    }
}

pub fn rule_input_system(
    mut commands: Commands,
    keyboard_input: Res<ButtonInput<KeyCode>>,
    mut rule: ResMut<LifeRule>,
    nhood: ResMut<NSelect>,
    mut query: Query<(Entity, &mut Text, &mut NTimer), With<RText>>,
    time: Res<Time>,
) {
    if keyboard_input.just_pressed(KeyCode::KeyH) ||
        keyboard_input.just_pressed(KeyCode::KeyJ) ||
        keyboard_input.just_pressed(KeyCode::KeyK) ||
        keyboard_input.just_pressed(KeyCode::KeyL) ||
        keyboard_input.just_pressed(KeyCode::KeyV)
    {
        for (entity, _, _) in query.iter() {
            commands.entity(entity).despawn();
        }
        if keyboard_input.just_pressed(KeyCode::KeyH) && !(keyboard_input.pressed(KeyCode::ShiftRight) || keyboard_input.pressed(KeyCode::ShiftLeft)) {
            rule.die_lower = if rule.die_lower > 0 {rule.die_lower-1} else {0}
        } else if keyboard_input.just_pressed(KeyCode::KeyJ) && !(keyboard_input.pressed(KeyCode::ShiftRight) || keyboard_input.pressed(KeyCode::ShiftLeft)) {
            rule.die_lower = if rule.die_lower < rule.die_upper {rule.die_lower+1} else {rule.die_upper}
        } else if keyboard_input.just_pressed(KeyCode::KeyH) && (keyboard_input.pressed(KeyCode::ShiftRight) || keyboard_input.pressed(KeyCode::ShiftLeft)) {
            rule.die_upper = if rule.die_upper > rule.die_lower {rule.die_upper-1} else {rule.die_lower}
        } else if keyboard_input.just_pressed(KeyCode::KeyJ) && (keyboard_input.pressed(KeyCode::ShiftRight) || keyboard_input.pressed(KeyCode::ShiftLeft)) {
            rule.die_upper = if rule.die_upper < nhood.0.max() {rule.die_upper+1} else {nhood.0.max()}
        } else if keyboard_input.just_pressed(KeyCode::KeyK) {
            rule.live = if rule.live > 1 {rule.live-1} else {1}
        } else if keyboard_input.just_pressed(KeyCode::KeyL) {
            rule.live = if rule.live < nhood.0.max() {rule.live+1} else {nhood.0.max()}
        }
        commands.spawn(
            TextBundle::from(rule.string().as_str())
                .with_style(
                    Style {
                        position_type: PositionType::Absolute,
                        top: Val::Px(5.0),
                        right: Val::Px(15.0),
                        ..default()
                    }
                )
            )
            .insert(RText)
            .insert(NTimer(Timer::from_seconds(NTIME, TimerMode::Once)));
    } else {
        for (entity, mut text, mut timer) in query.iter_mut() {
            timer.0.tick(time.delta());
            if timer.0.finished() {
                commands.entity(entity).despawn();
            } else {
                text.sections[0].style.color = Color::Rgba {
                    red: 1.0,
                    green: 1.0,
                    blue: 1.0,
                    alpha: 1.0 * ( timer.0.fraction_remaining() ),
                };
            }
        }
    }
}

pub fn click_to_spawn(
    mut commands: Commands,
    wnd_query: Query<&Window, With<PrimaryWindow>>,
    camera_query: Query<(&Transform, &OrthographicProjection), With<OrthographicProjection>>,
    mut mouse: ResMut<ButtonInput<MouseButton>>,
    query: Query<(Entity, &Coord), With<LiveCell>>,
    mut grid: ResMut<Grid>,
    mut painting: ResMut<Painting>,
) {
    let Some(cp) = wnd_query.single().cursor_position() else {return};
    let Ok(primary_window) = wnd_query.get_single() else {return};
    let (camera_transform, camera_projection) = camera_query.single();
    let wnd_size = Vec2 {x: primary_window.width(), y: primary_window.height()};

    let grid_vec = (((cp - wnd_size/2.0)*Vec2 { x: 1.0, y: -1.0}*camera_projection.scale + camera_transform.translation.truncate())/GRID_SIZE).round();

    if mouse.just_pressed(MouseButton::Left) {
        mouse.clear_just_pressed(MouseButton::Left);
        if grid.0.contains(&grid_vec.to_gridcoord()) {
            // is pressed an existing, despawn
            for (entity, coord) in query.iter() {
                if coord.pos == grid_vec {
                    commands.entity(entity).despawn();
                    grid.0.remove(&grid_vec.to_gridcoord());
                }
            }
            // activate erasing
            painting.0 = false;
        } else {
            commands.spawn(LiveCell)
                .insert(Coord { pos: grid_vec })
                .insert(SpriteBundle {
                    sprite: Sprite {
                        color: SPAWN_COLOR,
                        ..default()
                    },
                    transform: Transform {
                        translation: (grid_vec*GRID_SIZE).extend(0.0),
                        scale: GRID_SIZE.extend(1.0),
                        ..default()
                    },
                    ..default()
                });
            grid.0.insert(grid_vec.to_gridcoord());
            // activate painting
            painting.0 = true;
        }
    } else if mouse.pressed(MouseButton::Left) {
        if !painting.0 {
            // despawning if present
            for (entity, coord) in query.iter() {
                if coord.pos == grid_vec {
                    commands.entity(entity).despawn();
                    grid.0.remove(&grid_vec.to_gridcoord());
                }
            }
        } else {
            // spawning if not already present
            if !grid.0.contains(&grid_vec.to_gridcoord()) {
                commands.spawn(LiveCell)
                    .insert(Coord { pos: grid_vec })
                    .insert(SpriteBundle {
                        sprite: Sprite {
                            color: SPAWN_COLOR,
                            ..default()
                        },
                        transform: Transform {
                            translation: (grid_vec*GRID_SIZE).extend(0.0),
                            scale: GRID_SIZE.extend(1.0),
                            ..default()
                        },
                        ..default()
                    });
                grid.0.insert(grid_vec.to_gridcoord());
            }
        }
    }
}
