
use std::collections::HashSet;

use bevy::prelude::*;

mod camera;
mod components;
mod consts;
mod input;
mod life;
mod neighbours;
mod resources;

use crate::camera::setup_camera;

use crate::input::{
    Painting,
    camera_input_system,
    speed_control_input_system,
    game_input_system,
    click_to_spawn,
    neighbourhood_input_system,
    rule_input_system,
};

use crate::life::{
    update_system,
    spawn_cells
};

use crate::neighbours::Neighbourhood;

use crate::resources::{
    Grid,
    Pause,
    NSelect,
    LifeRule,
};

pub const TIME_STEP: f64 = 0.1;

type GridCoord = (i32, i32);

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .insert_resource(ClearColor(Color::Rgba { red: 0.1, green: 0.1, blue: 0.1, alpha: 1.0 }))
        .insert_resource(Grid(HashSet::new()))
        .insert_resource(Pause(false))
        .insert_resource(Painting(true))
        .insert_resource(Time::<Fixed>::from_seconds(TIME_STEP))
        .insert_resource(NSelect(Neighbourhood::Moore))
        .insert_resource(LifeRule::new())
        .add_systems(
            Startup,
            (
                setup_camera,
                spawn_cells,
            )
        )
        .add_systems(
            FixedUpdate,
            (
                update_system,
            )
        )
        .add_systems(
            Update,
            (
                camera_input_system,
                speed_control_input_system,
                game_input_system,
                click_to_spawn,
                neighbourhood_input_system,
                rule_input_system,
            )
        )
        .run();
}
