
use bevy::prelude::*;

use std::collections::HashSet;

use crate::consts::{
    SPAWN_COLOR,
    GRID_SIZE,
};

use crate::components::{
    LiveCell,
    Coord,
};

use crate::neighbours::GridVector;

use crate::resources::{
    Grid,
    NSelect,
    Pause,
    LifeRule,
};

use crate::GridCoord;

fn get_occupied_cells() -> Vec<GridCoord> {
    // glider
    vec![
                (2,3),  (3,3),
        (1,2),          (3,2),
                        (3,1)
    ]
}

pub fn spawn_cells(mut commands: Commands, mut grid: ResMut<Grid>) {
    for cell in get_occupied_cells() {
        let pos = Vec2::new(cell.0 as f32, cell.1 as f32);
        commands.spawn(LiveCell)
            .insert(Coord { pos: pos })
            .insert(SpriteBundle {
                sprite: Sprite {
                    color: SPAWN_COLOR,
                    ..default()
                },
                transform: Transform {
                    translation: (pos*GRID_SIZE).extend(0.0),
                    scale: GRID_SIZE.extend(42.0),
                    ..default()
                },
                ..default()
            });
        grid.0.insert(cell);
    }
}

pub fn update_system(
    mut commands: Commands,
    mut query: Query<(Entity, &Coord, &mut Sprite), With<LiveCell>>,
    mut grid: ResMut<Grid>,
    nhood: Res<NSelect>,
    pause: Res<Pause>,
    life_rule: Res<LifeRule>,
) {
    if pause.0 {
        return ()
    }
    let mut empty_neighbours = HashSet::<GridCoord>::new();
    let mut to_kill = HashSet::<GridCoord>::new();
    let mut to_spawn = HashSet::<GridCoord>::new();
    for (entity, coord, mut sprite) in query.iter_mut() {
        let n = coord.pos.neighbourhood(&nhood.0).iter()
            .map(|e| {
                if grid.0.contains(e) {
                    1
                } else {
                    empty_neighbours.insert(*e);
                    0
                }
            })
            .sum::<i32>();
        if n < life_rule.die_lower {
            // 1. Any live cell with fewer than two live neighbours dies, as if by underpopulation.
            to_kill.insert(coord.pos.to_gridcoord());
            commands.entity(entity).despawn();
        } else if n > life_rule.die_upper {
            // 3. Any live cell with more than three live neighbours dies, as if by overpopulation.
            to_kill.insert(coord.pos.to_gridcoord());
            commands.entity(entity).despawn();
        } else {
            // 2. Any live cell with two or three live neighbours lives on to the next generation.
            let [r, g, b, _] = sprite.color.as_rgba_f32();
            sprite.color.set_r(if r*1.1 > 1.0 { 1.0 } else { if r == 0.0 { 0.1 } else {r * 1.1} });
            sprite.color.set_g(if g*1.1 > 1.0 { 1.0 } else { if g == 0.0 { 0.1 } else {g * 1.1} });
            sprite.color.set_b(if b*1.1 > 1.0 { 1.0 } else { if b == 0.0 { 0.1 } else {b * 1.1} });
        }
    }

    for coord in empty_neighbours.iter() {
        let n = Vec2::new(coord.0 as f32, coord.1 as f32).neighbourhood(&nhood.0).iter()
            .map(|e| {
                if grid.0.contains(e) {
                    1
                } else {
                    0
                }
            })
            .sum::<i32>();
        if n == life_rule.live {
            // 4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
            to_spawn.insert(*coord);
        }
    }

    // spawn new entries
    for coord in to_spawn.iter() {
        let pos = Vec2::new(coord.0 as f32, coord.1 as f32);
        commands.spawn(LiveCell)
            .insert(Coord { pos: pos })
            .insert(SpriteBundle {
                sprite: Sprite {
                    color: SPAWN_COLOR,
                    ..default()
                },
                transform: Transform {
                    translation: (pos*GRID_SIZE).extend(0.0),
                    scale: GRID_SIZE.extend(42.0),
                    ..default()
                },
                ..default()
            });
        grid.0.insert(*coord);
    }
    // remove dead entries from grid
    for coord in to_kill.iter() {
        grid.0.remove(coord);
    }
}
