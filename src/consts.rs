use bevy::prelude::{
    Color,
    Vec2
};

pub const GRID_SIZE: Vec2 = Vec2::from_array([10., 10.]);

pub const SPAWN_COLOR: Color = Color::MAROON;
