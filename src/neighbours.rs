
use bevy::prelude::*;

use std::{
    str::FromStr,
    collections::HashSet,
};

use crate::GridCoord;

#[allow(dead_code)]
pub enum Neighbourhood {
    VonNeumann1,
    Diagonal,
    Moore,
    VonNeumann2,
}

impl Neighbourhood {
    pub fn string(&self) -> String {
        return match self {
            Neighbourhood::VonNeumann1 => {String::from_str("VonNeumann1").unwrap()},
            Neighbourhood::Diagonal => {String::from_str("Diagonal").unwrap()},
            Neighbourhood::Moore => {String::from_str("Moore").unwrap()},
            Neighbourhood::VonNeumann2 => {String::from_str("VonNeumann2").unwrap()},
        };
    }
    pub fn max(&self) -> i32 {
        return match self {
            Neighbourhood::VonNeumann1 => {4},
            Neighbourhood::Diagonal => {4},
            Neighbourhood::Moore => {8},
            Neighbourhood::VonNeumann2 => {12},
        };
    }
}

pub trait CyclicEnum {
    fn next(&mut self);
}

impl CyclicEnum for Neighbourhood {
    fn next(&mut self) {
        *self = match self {
            Neighbourhood::VonNeumann1 => {Neighbourhood::Diagonal},
            Neighbourhood::Diagonal => {Neighbourhood::Moore},
            Neighbourhood::Moore => {Neighbourhood::VonNeumann2},
            Neighbourhood::VonNeumann2 => {Neighbourhood::VonNeumann1},
        };
    }
}

pub trait GridVector {
    fn to_gridcoord(&self) -> GridCoord;
    fn neighbourhood(&self, n: &Neighbourhood) -> HashSet<GridCoord>;
    fn vn1_neighbourhood(&self) -> HashSet<GridCoord>;
    fn d_neighbourhood(&self) -> HashSet<GridCoord>;
    fn moore_neighbourhood(&self) -> HashSet<GridCoord>;
    fn vn2_neighbourhood(&self) -> HashSet<GridCoord>;
}

impl GridVector for Vec2 {
    fn to_gridcoord(&self) -> GridCoord {
        (self.x as i32, self.y as i32)
    }
    fn neighbourhood(&self, n: &Neighbourhood) -> HashSet<GridCoord> {
        match n {
            Neighbourhood::VonNeumann1 => {self.vn1_neighbourhood()},
            Neighbourhood::Diagonal => {self.d_neighbourhood()},
            Neighbourhood::Moore => {self.moore_neighbourhood()},
            Neighbourhood::VonNeumann2 => {self.vn2_neighbourhood()},
        }
    }
    fn vn1_neighbourhood(&self) -> HashSet<GridCoord> {
        let mut neighbourhood = HashSet::<GridCoord>::new();
        neighbourhood.insert((*self + Vec2::X).to_gridcoord());
        neighbourhood.insert((*self - Vec2::X).to_gridcoord());
        neighbourhood.insert((*self + Vec2::Y).to_gridcoord());
        neighbourhood.insert((*self - Vec2::Y).to_gridcoord());
        neighbourhood
    }
    fn d_neighbourhood(&self) -> HashSet<GridCoord> {
        let mut neighbourhood = HashSet::<GridCoord>::new();
        neighbourhood.insert((*self + Vec2::ONE).to_gridcoord());
        neighbourhood.insert((*self - Vec2::ONE).to_gridcoord());
        neighbourhood.insert((*self + Vec2::X - Vec2::Y).to_gridcoord());
        neighbourhood.insert((*self - Vec2::X + Vec2::Y).to_gridcoord());
        neighbourhood
    }
    fn moore_neighbourhood(&self) -> HashSet<GridCoord> {
        let mut neighbourhood = HashSet::<GridCoord>::new();
        neighbourhood.insert((*self + Vec2::X).to_gridcoord());
        neighbourhood.insert((*self - Vec2::X).to_gridcoord());
        neighbourhood.insert((*self + Vec2::Y).to_gridcoord());
        neighbourhood.insert((*self - Vec2::Y).to_gridcoord());
        neighbourhood.insert((*self + Vec2::ONE).to_gridcoord());
        neighbourhood.insert((*self - Vec2::ONE).to_gridcoord());
        neighbourhood.insert((*self + Vec2::X - Vec2::Y).to_gridcoord());
        neighbourhood.insert((*self - Vec2::X + Vec2::Y).to_gridcoord());
        neighbourhood
    }
    fn vn2_neighbourhood(&self) -> HashSet<GridCoord> {
        let mut neighbourhood = HashSet::<GridCoord>::new();
        neighbourhood.insert((*self + Vec2::X).to_gridcoord());
        neighbourhood.insert((*self - Vec2::X).to_gridcoord());
        neighbourhood.insert((*self + Vec2::Y).to_gridcoord());
        neighbourhood.insert((*self - Vec2::Y).to_gridcoord());
        neighbourhood.insert((*self + Vec2::X + Vec2::X).to_gridcoord());
        neighbourhood.insert((*self - Vec2::X - Vec2::X).to_gridcoord());
        neighbourhood.insert((*self + Vec2::Y + Vec2::Y).to_gridcoord());
        neighbourhood.insert((*self - Vec2::Y - Vec2::Y).to_gridcoord());
        neighbourhood.insert((*self + Vec2::ONE).to_gridcoord());
        neighbourhood.insert((*self - Vec2::ONE).to_gridcoord());
        neighbourhood.insert((*self + Vec2::X - Vec2::Y).to_gridcoord());
        neighbourhood.insert((*self - Vec2::X + Vec2::Y).to_gridcoord());
        neighbourhood
    }
}
