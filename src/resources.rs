
use bevy::prelude::Resource;

use std::collections::HashSet;

use crate::GridCoord;

use crate::neighbours::Neighbourhood;

pub const DIE_LOWER: i32 = 2;
pub const DIE_UPPER: i32 = 3;
pub const LIVE: i32 = 3;

#[derive(Resource)]
pub struct LifeRule {
    pub die_lower: i32,
    pub die_upper: i32,
    pub live: i32
}

impl LifeRule {
    pub fn new() -> LifeRule {
        LifeRule {
            die_lower: DIE_LOWER,
            die_upper: DIE_UPPER,
            live: LIVE
        }
    }
    pub fn string(&self) -> String {
        return format!("({}, {}) : {}", self.die_lower, self.die_upper, self.live);
    }
}

#[derive(Resource)]
pub struct Grid(pub HashSet<GridCoord>);

#[derive(Resource)]
pub struct Pause(pub bool);

#[derive(Resource)]
pub struct NSelect(pub Neighbourhood);
