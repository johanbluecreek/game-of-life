use bevy::prelude::{
    Component,
    Vec2
};

#[derive(Component)]
pub struct LiveCell;

#[derive(Component)]
pub struct Coord {
    pub pos: Vec2
}
