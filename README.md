# Game of Life

Simple [Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) example exercise in [bevy](https://github.com/bevyengine/bevy/).

 * Pause the game by pressing `space`
 * Click an empty site to make it live, or a live one to make it empty, hold mouse button to paint/erase cells
 * Pan the camera with arrow-keys (or `wasd`), reset the camera by pressing `r`
 * Zoom the camera with the mouse scroll wheel
 * Use +/- to speed up/slow down the simulation
 * Clear the grid of all live cells by pressing `c`
 * Change neighbourhood used by pressing `n`. This will cycle neighbourhoods in this order:
   - [Moore](https://en.wikipedia.org/wiki/Moore_neighborhood), 3x3 grid (default)
   - [Von Neumann `r=2`](https://en.wikipedia.org/wiki/Von_Neumann_neighborhood), points of Manhattan-distance 2
   - [Von Neumann `r=1`](https://en.wikipedia.org/wiki/Von_Neumann_neighborhood), points of Manhattan-distance 1
   - Diagonal, only the points on the diagonal
 * Display the current neighbourhood and the rules of life by pressing `v`
   - The rules of life will be displayed as: `(die_lower, die_upper) : live`
     * Any live cell with fewer than `die_lower` neighbours will die as if by underpopulation
     * Any live cell with more than `die_upper` neighbours will die as if by overpopulation
     * Any dead cell with `live` neighbours will become live, as if by reproduction
 * Modify the rules of life:
   - `h` and `j` lowers and raises `die_lower` (bound by `(0, die_upper)`)
   - `H` and `J` lowers and raises `die_upper` (bound by `(die_lower, max neighbourhood size)`)
   - `k` and `l` lowers and raises `live` (bound by `(1, max neighbourhood size)`)
 * The game starts with a ["glider"](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Examples_of_patterns) traveling northeast
